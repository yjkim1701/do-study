var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var morgan = require('morgan');
var config = require('./config/config');

var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

// connect mongodb
mongoose
  .connect(
    config.db,
    {
      promiseLibrary: require('bluebird')
    }
  )
  .then(() => console.log('connection successful'))
  .catch(err => console.log(err));

var app = express();

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(cors());

app.use(express.static('public'));

// route module 사용
require('./routes')(app);
require('./controllers/GreetingController')(app);

app.listen(config.port || 3001);
