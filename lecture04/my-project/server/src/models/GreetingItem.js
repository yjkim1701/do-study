var mongoose = require('mongoose');

var GreetingItemSchema = new mongoose.Schema({
  id: String,
  userName: String,
  greetingMessage: String,
  greetingTime: {
    type: Date,
    default: Date.Now
  }
});

module.exports = mongoose.model('GreetingItemSchema', GreetingItemSchema);
