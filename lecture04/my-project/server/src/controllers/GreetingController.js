var GreetingItemModel = require('../models/GreetingItem');

module.exports = app => {
  app.get('/api/greeting', (req, res, next) => {
    try {
      GreetingItemModel.find((err, greetingItemList) => {
        if (err) return next(err);

        res.json(greetingItemList);
      });
    } catch (err) {
      res.status(400).send({
        error: 'an error has occured'
      });
      console.log(err);
    }
  });

  app.post('/api/greeting', (req, res, next) => {
    try {
      GreetingItemModel.create(req.body, (err, greetingItem) => {
        if (err) return next(err);

        res.json(greetingItem);
      });
    } catch (err) {
      res.status(400).send({
        error: 'an error has occured'
      });
    }
  });
};
